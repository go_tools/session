package main

import (
	"encoding/json"
	"gitlab.srv.pv.km/go-framework/session"
	"net/http"
)

type Handler struct {
	Session *session.Session
}

type REQUEST struct {
	Status       string      `json:"status"`
	CheckSession bool        `json:"checkSession"`
	Response     interface{} `json:"response"`
}

type Session_Data struct {
	IsValid bool
	Id      int
	Name    string
}

type IsValid struct {
	Data interface{}
}

func (self *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	var result interface{}

	request := &REQUEST{Status: `OK`}
	request.CheckSession, _ = self.Session.CheckSession(r, w)

	switch r.URL.Path {
	case "/favicon.ico":
		return
	case "/create_session":
		result, err = self.CreateSession(r, w)
	case "/logout":
		result, err = self.DeleteSession(r, w)
	case "/isValid":
		result, err = self.IsValid()
	case "/read":
		result, err = self.ReadSession()
	case "/write":
		result = self.WriteSession()
	}

	if err != nil {
		request.Status = `Error`
		request.Response = err.Error()
	} else {
		request.Response = result
	}

	output, _ := json.Marshal(request)
	w.Write(output)
}

func (self *Handler) CreateSession(r *http.Request, w http.ResponseWriter) (string, error) {
	Data := &Session_Data{Id: 1, Name: "Test"}
	err := self.Session.InitSession(r, w, Data)

	return `Session created!`, err
}

func (self *Handler) WriteSession() interface{} {
	Data := &Session_Data{Id: 2, Name: "Test_2", IsValid: true}
	self.Session.Write(Data)

	return `Session update`
}

func (self *Handler) ReadSession() (interface{}, error) {
	Data := &Session_Data{}
	err := self.Session.Read(Data)
	return Data, err
}

func (self *Handler) DeleteSession(r *http.Request, w http.ResponseWriter) (string, error) {
	err := self.Session.LogOut(r, w)
	return `Session deleted!`, err
}

func (self *Handler) IsValid() (interface{}, error) {
	Data := &Session_Data{}
	err := self.Session.Read(Data)
	return Data, err
}
