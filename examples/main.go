package main

import (
	"encoding/json"
	"fmt"
	"gitlab.srv.pv.km/go-framework/session"
	"gitlab.srv.pv.km/go-libs/config"
	"log"
	"net/http"
	"time"
)

type CONSTANT struct {
	Session *session.SessionConfig
}

type SessionStore struct {
	array map[string]string
}

// Set(string, interface{}, uint32) error
// Get(string, interface{}) error
// Update(string, interface{}, uint32) error
// Remove(string) error

func newSessionStore() *SessionStore {
	return &SessionStore{array: make(map[string]string)}
}

func (ts *SessionStore) Set(key string, data interface{}, time uint32) error {
	byte, err := json.Marshal(data)
	if err != nil {
		fmt.Println("SET  ", err.Error())
		return err
	}
	ts.array[key] = string(byte)
	fmt.Println(`Array :: `, ts.array[key])

	return nil
}

func (ts *SessionStore) Get(sessionId string, data interface{}) error {
	err := json.Unmarshal([]byte(ts.array[sessionId]), data)
	if err != nil {
		fmt.Println("GET  ", err.Error())
	}

	return err
}

func (ts *SessionStore) Update(key string, data interface{}, time uint32) error {
	return ts.Set(key, data, time)
}

func (ts *SessionStore) Remove(sessionId string) error {
	delete(ts.array, sessionId)
	return nil
}

func main() {
	const CONST = "constants.json"

	constant := &CONSTANT{}
	config.ReadFile(CONST, constant)

	sessionStore := newSessionStore()
	sessionHandler := session.NewSession(sessionStore, constant.Session)

	server := &http.Server{
		Addr:         ":8080",
		Handler:      &Handler{Session: sessionHandler},
		ReadTimeout:  3 * time.Second,
		WriteTimeout: 3 * time.Second,
	}
	server.SetKeepAlivesEnabled(false)
	log.Fatal(server.ListenAndServe())
}
