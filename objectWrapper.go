package session

import (
	"net/http"
	"time"
)

type wrapper struct {
	config *SessionConfig
}

func newCookie(conf *SessionConfig) *wrapper {
	return &wrapper{config: conf}
}

func (ck *wrapper) Write(r *http.Request, rw http.ResponseWriter, value string) error {
	cookie, err := r.Cookie(ck.config.Name)
	if err != nil || cookie.Value == "" {
		cookie = &http.Cookie{
			Name:    ck.config.Name,
			Value:   value,
			Expires: time.Now().Add(time.Duration(ck.config.Expires) * time.Minute),
		}
	}

	http.SetCookie(rw, cookie)
	return nil
}

func (ck *wrapper) Read(r *http.Request, rw http.ResponseWriter) (string, error) {
	cookie, err := r.Cookie(ck.config.Name)
	if err != nil {
		return ``, err
	}
	cookie.Expires = time.Now().Add(time.Duration(ck.config.Expires) * time.Minute)
	http.SetCookie(rw, cookie)
	return cookie.Value, nil
}

func (ck *wrapper) Remove(r *http.Request, rw http.ResponseWriter) {
	cookie, err := r.Cookie(ck.config.Name)
	if err != nil || cookie.Value == "" {
		return
	}

	cookie.Expires = time.Now()
	cookie.MaxAge = -1
	cookie.Value = ``
	http.SetCookie(rw, cookie)
}
