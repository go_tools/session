package session

import (
	"errors"
	"gitlab.srv.pv.km/go-libs/utils"
	"net/http"
	"time"
)

// Объект сессии
type Session struct {
	store     sessionAction
	config    *SessionConfig
	action    objectAction
	sessionId string
	isSet     bool
}

// Конфигурация объекта сессии
type SessionConfig struct {
	Type    string `json:"Type"`
	Name    string `json:"CookieName"`
	Expires int    `json:"Expires"`
	Length  int    `json:"Length"`
}

// Интерфейс объекта хранилища
type sessionAction interface {
	Set(string, interface{}, uint32) error
	Get(string, interface{}) error
	Update(string, interface{}, uint32) error
	Remove(string) error
}

// Прослойка для работы с куками
type objectAction interface {
	Write(*http.Request, http.ResponseWriter, string) string
	Read(*http.Request, http.ResponseWriter) (string, error)
	Remove(*http.Request, http.ResponseWriter)
}

// Создание объекта сессии
func NewSession(cb sessionAction, sConf *SessionConfig) *Session {
	s := new(Session)
	s.store = cb
	s.config = sConf
	s.action = newCookie(sConf)
	return s
}

// Получить идентификатор сессии входящего запроса
func (s *Session) CheckSession(r *http.Request, rw http.ResponseWriter) (bool, error) {
	var err error
	if s.sessionId, err = s.action.Read(r, rw); err != nil {
		return false, err
	}
	s.isSet = len(s.sessionId) > 0
	if err = s.isValid(); err != nil {
		return false, err
	}
	return s.isSet, err
}

// Первичная инициализация объекта сессии
func (s *Session) InitSession(r *http.Request, rw http.ResponseWriter, data interface{}) error {
	s.sessionId = s.action.Write(r, rw, utils.GenerateId(s.config.Length))
	return s.set(data, uint32(s.config.Expires*60))
}

// Прочитать данные сессии
func (s *Session) Read(data interface{}) error {
	if err := s.isValid(); err != nil {
		return err
	}
	return s.get(data)
}

// Запись данных в сессию
func (s *Session) Write(data interface{}) error {
	if err := s.isValid(); err != nil {
		return err
	}
	return s.update(data)
}

// Завершение работы с сессией
func (s *Session) LogOut(r *http.Request, rw http.ResponseWriter) error {
	return s.remove(r, rw)
}

func (s *Session) isValid() (err error) {
	if !s.isSet {
		err = errors.New(`Session not found`)
	}
	return s.store.Get(s.sessionId, nil)
}

// Запись объекта storeInfo в хранилище
func (s *Session) set(value interface{}, TTL uint32) error {
	return s.store.Set(s.sessionId, value, TTL)
}

// Чтение объекта storeInfo из хранилища
func (s *Session) get(data interface{}) error {
	return s.store.Get(s.sessionId, data)
}

// Чтение объекта storeInfo из хранилища
func (s *Session) update(data interface{}) error {
	return s.store.Update(s.sessionId, data, uint32(time.Duration(500)*time.Millisecond))
}

// Записываем в хранилище объект storeInfo со свойством IsValid в состоянии flase
func (s *Session) remove(r *http.Request, rw http.ResponseWriter) error {
	s.action.Remove(r, rw)
	return s.store.Remove(s.sessionId)
}
