Установка
--------
```go get -insecure gitlab.srv.pv.km/go-framework/session```

Импорт
--------
```
import "gitlab.srv.pv.km/go-framework/session"
```
Пакет отвечает за логику работы сессии


Создание объекта сессии
--------
```
NewSession(cb sessionAction, sConf *SessionConfig) *Session
```

В качестве параметров:
Первым аргументом передается объект, в котором реализован интерфейс:
```
type sessionAction interface {
	Set(string, interface{}, uint32) error
	Get(string, interface{}) error
	Update(string, interface{}, uint32) error
	Remove(string) error
}
```
Вторым аргументом передаются конфигурационные настройки сессии:
```
type SessionConfig struct {
	Type    string `json:"Type"`
	Name    string `json:"CookieName"`
	Expires int    `json:"Expires"`
	Length  int    `json:"Length"`
}
```
Type - тип работы с сессие. Пока реализована работа с куками
Name - название сессии
Expires - время хранения обекта сессии
Length - длина создаваемого идентификатора сессии

Выходные параметры:
errod - объект ошибки


Первичная инициализация объекта сессии
--------
```
(*Session) InitSession(r *http.Request, rw http.ResponseWriter, data interface{}) error {
```
В качестве параметров:
r - объект запроса
w - объект ответа
data - пользовательский объект данных сессии

Выходные параметры:
error - объект ошибки


Инициализация сессии
--------
```
(*Session) CheckSession(r *http.Request, rw http.ResponseWriter) (bool, error)
```
В качестве параметров:
r - объект запроса
w - объект ответа

Метод проверяет, является ли поступивший запрос - авторизованный.

Выходные параметры:
bool : true - авторизованный, false - неавторизованный
errod - объект ошибки


Чтение объекта данных из сессии
--------
```
(*Session) Read(data interface{}) error
```
В качестве параметров принимает:
data - объект для записи ответа

Выходные параметры:
error - объект ошибки


Запись объекта данных в сессию
--------
```
(*Session) Write(data interface{}) error 
```
В качестве параметров принимает:
data - объект данных 

Выходные параметры:
error - объект ошибки


Завершение сеанса
--------
```
(*Session) LogOut(r *http.Request, rw http.ResponseWriter) error
```
В качестве параметров:
r - объект запроса
w - объект ответа

Выходные параметры:
error - объект ошибки

______
Примеры подробнее в разделе examples